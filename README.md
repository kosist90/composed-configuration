# README #

**composed-configuration** is a malleable by-reference interface for reading and writing application configuration (think *.ini files). This base package includes a default implementation for INI files but can be extended by dependency injection to use whatever persistence type your application requires (database, XML file, etc.).

## Malleable VI Caveats and Warnings

*The level of malleablity required for this API only exists in LabVIEW 2017 **Service Pack 1** and later.* You will find no success using this package in LV2017 pre-service-pack or any prior LabVIEW version.

### How do I get set up?
- Install this package using [GPM](https://gpackage.io) and start coding
- All public API methods are clearly marked in the souce libraries (if you are calling non-API methods from souce, you're doing it wrong).

#### Examples
- No explicit examples provided at this time, but the automated tests located in the open-source repo (linked by GPM) illustrate basic use of the API

#### Dependencies
- This package depends only on base *vi.lib*

### Contribution guidelines
- All contributions must adhere to SOLID design principles.
- All code must be unit tested to the extent reasonably possible.
- Please contact the author if you want to contribute.

### Who do I talk to?
- Ethan Stern | Composed Systems, LLC
- ethan.stern@composed.io

### License
- See license file