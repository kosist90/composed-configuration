﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="17008000">
	<Property Name="EndevoGOOP_ClassItemIcon" Type="Str">BlueCircle</Property>
	<Property Name="EndevoGOOP_ClassProvider" Type="Str">Endevo LabVIEW Native</Property>
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">1179848</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">12124142</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorProtected" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">9341183</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ConnectorPanePattern" Type="Str">0</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="EndevoGOOP_PlugIns" Type="Str"></Property>
	<Property Name="EndevoGOOP_TemplateUsed" Type="Str">NativeSubTemplate_4x4x4</Property>
	<Property Name="EndevoGOOP_TemplateVersion" Type="Str">1.0.0.1</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!,J!!!*Q(C=T:;`&lt;B."%)@H%II5#/3/DMD+!S".B93IX&amp;`FHGJ[+C-B*6O[1++?6\$]"G[AHS[V&amp;?5&amp;X+2!(!&lt;T?&lt;X_=Y[$GS"FTX-Z`X[TM^`NLF=2S?V-Z*EMDL7,UHCE4Y\N1^N@30YM6I&lt;MF#A0O?NC@4PM\T`&gt;[\]?_I"`K*&amp;]@3U0N5@RN[WJG_:BP[G8%^8+W0&amp;8\F\'RN_[L9S.8\@]_8W`W6;9T_@V!&lt;^ET/=0_G1U]X`[R8Z[@N0SGW9](L@HP[YX0K]ZTKW^@P8+XX(XVT^H8)SX^P\_E,LFXP&gt;J/_Z"P^7/_U=;/?FIUH^O)N8[&amp;RW%%U&lt;I[LA)^%!0^%!0^%"X&gt;%&gt;X&gt;%&gt;X&gt;%=X&gt;%-X&gt;%-X&gt;%.8&gt;%68&gt;%68&gt;&amp;W?*MM,8&gt;!&amp;843@--(AQ5""U;"!E!S+",?!*_!*?!)?PEL!%`!%0!&amp;0Q%/+"$Q"4]!4]!1]&gt;*/!*_!*?!+?A)&gt;3G34SB1Z0Q%.Z=8A=(I@(Y8&amp;Y'&amp;)=(A@!'=QJ\"1"1RT4?8"Y("[(BU&gt;R?"Q?B]@B=8CQR?&amp;R?"Q?B]@BI5O?&amp;=]USQM&gt;(MK)Q70Q'$Q'D]&amp;$;4&amp;Y$"[$R_!R?"B/$"[$RY!Q"D3+AS"'*S0"_',Q'$T]%90(Y$&amp;Y$"[$"SOPE/7:7&gt;)M,X2Y&amp;"[&amp;2_&amp;2?"1?3ID#I`!I0!K0QE.:58A5(I6(Y6&amp;Y'%I5(I6(Y6&amp;!F%%:8J2C3E=F32%5(DZZNWB?*=]EGK`SUVRN6#E&lt;5-L'EL*BJ'Q%+1MM:?'E,)C5C:9SA6)G2MI,3XE2+9"3"J:35%KCT,B0C1ER)I&lt;%A/A40;*,&gt;*:&gt;(TFR.JP*&gt;$K6S71CI^&amp;)BM/B$!9$[@@\UOPVJ.PN3K@4W:R7ZVSL6KX0J@4RV&gt;@U?8"T^_MN=5FU&lt;O\1PZ#TDH4SL5JH&lt;[JU`LR+[&lt;2+1HRY5;60\[NU_;&gt;+6Q&gt;CM&lt;B.LX``4#^`X+&lt;4G_^*LODXDJ!=KX0J+:S.=L,[FWE\2X]"J&amp;:7@A!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.1</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.CoreWirePen" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!6*0%.M&gt;8.U:8)_$1I]4G&amp;N:4Z1:7Y],UZB&lt;75_$1I]4H6N27RU=TYY0#^/&gt;7V&amp;&lt;(2T0AU+0&amp;5T-DY.#DR/97VF0E:P=G6H=G^V&lt;G1A1W^M&lt;X)],UZB&lt;75_$1I]6G&amp;M0DEX.$!Z-D1],V:B&lt;$Y.#DQP64-S0AU+0&amp;5T-DY.#DR/97VF0E*B9WNH=G^V&lt;G1A1W^M&lt;X)],UZB&lt;75_$1I]6G&amp;M0DAW/41W.D)],V:B&lt;$Y.#DQP64-S0AU+0%.M&gt;8.U:8)_$1I]4G&amp;N:4Z';7RM)&amp;"B&gt;(2F=GY],UZB&lt;75_$1I]4H6N27RU=TYY0#^/&gt;7V&amp;&lt;(2T0AU+0&amp;5Y0AU+0%ZB&lt;75_5G^X)$!],UZB&lt;75_$1I]6G&amp;M0D)V.4QP6G&amp;M0AU+0#^6/$Y.#DR6/$Y.#DR/97VF0F*P&gt;S!R0#^/97VF0AU+0&amp;:B&lt;$YS.45],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A-DQP4G&amp;N:4Y.#DR797Q_-D5V0#^797Q_$1I],V5Y0AU+0&amp;5Y0AU+0%ZB&lt;75_5G^X)$-],UZB&lt;75_$1I]6G&amp;M0D)V.4QP6G&amp;M0AU+0#^6/$Y.#DR6/$Y.#DR/97VF0F*P&gt;S!U0#^/97VF0AU+0&amp;:B&lt;$YS.45],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A.4QP4G&amp;N:4Y.#DR797Q_-D5V0#^797Q_$1I],V5Y0AU+0&amp;5Y0AU+0%ZB&lt;75_5G^X)$9],UZB&lt;75_$1I]6G&amp;M0D)V.4QP6G&amp;M0AU+0#^6/$Y.#DR6/$Y.#DR/97VF0F*P&gt;S!X0#^/97VF0AU+0&amp;:B&lt;$YS.45],V:B&lt;$Y.#DQP64A_$1I],U.M&gt;8.U:8)_$1I]34%W0AU+0%ZB&lt;75_6WFE&gt;'A],UZB&lt;75_$1I]6G&amp;M0D%],V:B&lt;$Y.#DQP34%W0AU+0%680AU+0%ZB&lt;75_47^E:4QP4G&amp;N:4Y.#DR$;'^J9W5_1W^Q?4QP1WBP;7.F0AU+0%.I&lt;WFD:4Z0=DQP1WBP;7.F0AU+0%.I&lt;WFD:4Z&amp;?'.M&gt;8.J&gt;G5A4X)],U.I&lt;WFD:4Y.#DR$;'^J9W5_1GFU)%.M:7&amp;S0#^$;'^J9W5_$1I]1WBP;7.F0EZP&gt;#"$&lt;X"Z0#^$;'^J9W5_$1I]1WBP;7.F0EZP&gt;#"0=DQP1WBP;7.F0AU+0%.I&lt;WFD:4Z/&lt;X)A28BD&lt;(6T;8:F)%^S0#^$;'^J9W5_$1I]1WBP;7.F0EZP&gt;#"#;81A1WRF98)],U.I&lt;WFD:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^&amp;6TY.#DR&amp;4$Y.#DR/97VF0F.U?7RF0#^/97VF0AU+0%.I&lt;WFD:4Z4&lt;WRJ:$QP1WBP;7.F0AU+0%.I&lt;WFD:4Z%98.I0#^$;'^J9W5_$1I]1WBP;7.F0E2P&gt;$QP1WBP;7.F0AU+0%.I&lt;WFD:4Z%98.I)%2P&gt;$QP1WBP;7.F0AU+0%.I&lt;WFD:4Z%98.I)%2P&gt;#"%&lt;X1],U.I&lt;WFD:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^&amp;4$Y.#DR&amp;4$Y.#DR/97VF0E:J&lt;'QA5H6M:4QP4G&amp;N:4Y.#DR$;'^J9W5_28:F&lt;C"0:'1],U.I&lt;WFD:4Y.#DR$;'^J9W5_6WFO:'FO:TQP1WBP;7.F0AU+0&amp;:B&lt;$YQ0#^797Q_$1I],U6-0AU+0%6-0AU+0%ZB&lt;75_27ZE)%.B=(-],UZB&lt;75_$1I]1WBP;7.F0E2F:G&amp;V&lt;(1],U.I&lt;WFD:4Y.#DR$;'^J9W5_2GRB&gt;$QP1WBP;7.F0AU+0&amp;:B&lt;$YQ0#^797Q_$1I],U6-0AU+0#^$&lt;(6T&gt;'6S0AU+!!!!!!</Property>
	<Property Name="NI.LVClass.EdgeWirePen" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!5Y0%.M&gt;8.U:8)_$1I]4G&amp;N:4Z1:7Y],UZB&lt;75_$1I]4H6N27RU=TYY0#^/&gt;7V&amp;&lt;(2T0AU+0&amp;5T-DY.#DR/97VF0E:P=G6H=G^V&lt;G1A1W^M&lt;X)],UZB&lt;75_$1I]6G&amp;M0D%T-4%Y.TQP6G&amp;M0AU+0#^6-T)_$1I]64-S0AU+0%ZB&lt;75_1G&amp;D;W&gt;S&lt;X6O:#"$&lt;WRP=DQP4G&amp;N:4Y.#DR797Q_.49W.T1R-4QP6G&amp;M0AU+0#^6-T)_$1I]1WRV=X2F=DY.#DR/97VF0E:J&lt;'QA5'&amp;U&gt;'6S&lt;DQP4G&amp;N:4Y.#DR/&gt;7V&amp;&lt;(2T0DA],UZV&lt;56M&gt;(-_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A-$QP4G&amp;N:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^6/$Y.#DR6/$Y.#DR/97VF0F*P&gt;S!R0#^/97VF0AU+0&amp;:B&lt;$YQ0#^797Q_$1I],V5Y0AU+0&amp;5Y0AU+0%ZB&lt;75_5G^X)$)],UZB&lt;75_$1I]6G&amp;M0D!],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A-TQP4G&amp;N:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^6/$Y.#DR6/$Y.#DR/97VF0F*P&gt;S!U0#^/97VF0AU+0&amp;:B&lt;$YQ0#^797Q_$1I],V5Y0AU+0&amp;5Y0AU+0%ZB&lt;75_5G^X)$5],UZB&lt;75_$1I]6G&amp;M0D!],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A.DQP4G&amp;N:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^6/$Y.#DR6/$Y.#DR/97VF0F*P&gt;S!X0#^/97VF0AU+0&amp;:B&lt;$YQ0#^797Q_$1I],V5Y0AU+0#^$&lt;(6T&gt;'6S0AU+0%ER.DY.#DR/97VF0F&gt;J:(2I0#^/97VF0AU+0&amp;:B&lt;$YT0#^797Q_$1I],UER.DY.#DR&amp;6TY.#DR/97VF0EVP:'5],UZB&lt;75_$1I]1WBP;7.F0E.P=(E],U.I&lt;WFD:4Y.#DR$;'^J9W5_4X)],U.I&lt;WFD:4Y.#DR$;'^J9W5_28BD&lt;(6T;8:F)%^S0#^$;'^J9W5_$1I]1WBP;7.F0E*J&gt;#"$&lt;'6B=DQP1WBP;7.F0AU+0%.I&lt;WFD:4Z/&lt;X1A1W^Q?4QP1WBP;7.F0AU+0%.I&lt;WFD:4Z/&lt;X1A4X)],U.I&lt;WFD:4Y.#DR$;'^J9W5_4G^S)%6Y9WRV=WFW:3"0=DQP1WBP;7.F0AU+0%.I&lt;WFD:4Z/&lt;X1A1GFU)%.M:7&amp;S0#^$;'^J9W5_$1I]6G&amp;M0D!],V:B&lt;$Y.#DQP26=_$1I]25Q_$1I]4G&amp;N:4Z4&gt;(FM:4QP4G&amp;N:4Y.#DR$;'^J9W5_5W^M;71],U.I&lt;WFD:4Y.#DR$;'^J9W5_2'&amp;T;$QP1WBP;7.F0AU+0%.I&lt;WFD:4Z%&lt;X1],U.I&lt;WFD:4Y.#DR$;'^J9W5_2'&amp;T;#"%&lt;X1],U.I&lt;WFD:4Y.#DR$;'^J9W5_2'&amp;T;#"%&lt;X1A2'^U0#^$;'^J9W5_$1I]6G&amp;M0D!],V:B&lt;$Y.#DQP25Q_$1I]25Q_$1I]4G&amp;N:4Z';7RM)&amp;*V&lt;'5],UZB&lt;75_$1I]1WBP;7.F0E6W:7YA4W2E0#^$;'^J9W5_$1I]1WBP;7.F0F&gt;J&lt;G2J&lt;G=],U.I&lt;WFD:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^&amp;4$Y.#DR&amp;4$Y.#DR/97VF0E6O:#"$98"T0#^/97VF0AU+0%.I&lt;WFD:4Z%:7:B&gt;7RU0#^$;'^J9W5_$1I]1WBP;7.F0E:M981],U.I&lt;WFD:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^&amp;4$Y.#DQP1WRV=X2F=DY.#A!!!!!</Property>
	<Property Name="NI.LVClass.FlattenedPrivateDataCTL" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!"AY5F.31QU+!!.-6E.$4%*76Q!!%^A!!!2A!!!!)!!!%\A!!!!M!!!!!3&gt;5:8.U37ZJ2GFM:6.F9X2J&lt;WZ$&lt;WZG;7&gt;V=G&amp;U;7^O,GRW9WRB=X-!!!#A&amp;Q#!!!!Q!!!)!!1!!!!!"!!$!$Q!P!!@1)!#!!!!!!%!!1!'`````Q!!!!!!!!!!!!!!!/B;?K?DM$R,NK3;9!G'N+!!!!!-!!!!%!!!!!#/!:&gt;?*U;Z4,E^0JP4XI4FV"W-W9]!MA4JA!G9\0B#@A!!%!!!!!!!F:\ZXT$KXU7X`1P5)@_P21%!!!$`````V"W-W9]!MA4JA!G9\0B#@A!!!"!(_`CR6Z0R/B'HV&amp;6)A"?^!!!!"!!!!!!!!!!H!!&amp;-6E.$!!!!!1!#6EF-1A!!!!"16%AQ!!!!"1!"!!%!!!!!!A!$!!!!!!)!!1!!!!!!)!!!!"BYH'.A9W"K9,D!!-3-1";4"J$VA5'!!1![)147!!!!2A!!!2BYH'.AQ!4`A1")-4)Q-&amp;U!UCRIYG!;RK9GQ'5O,LOAYMR1.\,#B)(OXA/EG5"S5$610T#&gt;!?)4[/:Q9$%&lt;!)!(+#9!!!!!!!Q!!6:*2&amp;-!!!!!!!-!!!'%!!!$&amp;(C=;W"E9-AUND#\!+3:A6C=I9%B/4]FF9M"S'?!A"&gt;-$"3$!+BZ7GDCBA=/JQ'"(L^]#ZD@`);HWU6&amp;I,F'29+J6+4&lt;2U7EUU?&amp;J:.&amp;Z=7@````.R`B/&gt;TNE80=U1;ENJM$+(\=295$R!(3,#$[@W!'3"8-P!#A;2Q.&amp;=I-*3S'"[)/(W]Q9929$$-S#JP^-JV!UY#U"*)&amp;=!?AK?=!/F,AY%/7\E9.),^X)IA%#P&amp;UBH")((@BU"%$]BF0A!TMZ)(ZAA0OPD#1!35K!JUG))N9''%7&gt;&lt;-&gt;&gt;^!!_]N""%*F1+A+#&amp;5!IH;!88#%)_YQ0$T8PL[X#R1/&lt;%BB\!$%$5!-CG.EL-@!S!#SE!F)VE,6WA$:4&amp;!R7&amp;S"W"_A&lt;!UE03,1I'2EM)?,L5;SEQEMR]BQBA'GDJ(B%J4&gt;!(5D3%Q7K(E#F+U#:#&gt;!W&gt;J!^A%IWQD)&amp;I#S,2H"$$$&lt;$MK_!(5,,NL:X]56+5D!;2K7L$G"/$GXQ-"!LTI!&amp;!Y!GP3#`!!!!)]!!!$=?*RT9'"AS$3W-'M!UMS-$!TC$!U-S@EJK1R)9!M$&lt;N$]2K$&lt;257EUU7&amp;J^N(2;,42Y7DG['&lt;%UCT&gt;,+IP0DT````VA0]5Q\Q&lt;TOR!WA]!\`LQ?9D!I@B_N?_PL=,*-[):+9$S#U-P'!R*C#72R)(!7&gt;`&amp;V&gt;U^Y(5=A*R=G["A9&amp;?&gt;5!NE!U!^Z!C[A!!!!!/&amp;Q'!%!!!"D%X,D!O-1!!!!!!!!Q8!)!!!!!%-4=O-!!!!!!/&amp;Q'!%!!!"D%X,D!O-1!!!!!!!!Q8!)!!!!!%-4=O-!!!!!!/&amp;Q'!%!!!"D%X,D!O-1!!!!!!!"1"!!!!^6=VAHEGIIQO=V*/"EEZH1!!!!U!!!!!!!!!!!!!!!!!!!!!!!!!A0````_!=2C0A(NXX`0\/&gt;`T_X\@]`M2X`0```_-!!!"D!!!!9Q%!!'!#]!"A")9!9!C!Y'!1A'"A))$A9'#"I'"]A;"A@Y-A9(`W)'"``#"A@`QA9(``)'"``?"A@`R!9(`]A'"``9"A0`E!9!@[!'!!@!"A!!A!9!!!!(`````!!!#!0`````````````````````Q!!!!$`]!$Q!0]!$Q!0``]!!!!!``]0]0`Q```Q``````!0````$`!0`Q$`]0`````Q$````Q`Q````$`$`````]!````]0]!$Q!0`Q``````!0`````````````````Q!#)!!!!!!!!!!!!!!!!0]!!C!!!!!!!!!!!!!!!!$`!!)A!!!-,-!!!!!!!!!!`Q!!!!!!TG:G,-!!!!!!!0]!!!!!$#:G:G:CT!!!!!$`!!!!!-*G:G:G:G&lt;N!!!!`Q!!!!!G:G:G:G:G9A!!!0]!!!!/:G:G:G:G:O9!!!$`!!!!ZG:G:G:G:GZG!!!!`Q!!!/\O:G:G:G&lt;G:A!!!0]!!!$G:G\G:G:O:G9!!!$`!!!!*G:G:OZGZG:G!!!!`Q!!!#:G:G:G9G:G:A!!!0]!!!!G:G:G:GZG:G)!!!$`!!!!*G:G:G:O:G:C!!!!`Q!!!#:G:G:G&lt;G:G9A!!!0]!!!!G:G:G:GZG:GQ!!!$`!!!!*G:G:G:O:G&lt;!!!!!`Q!!!':G:G:G&lt;G:C!!!!!0]!!!$.ZG:G:GZG)!!!!!$`!!!!!!T?:G:O9M!!!!!!`Q!!!!!!!-XG&lt;GQ!!!!!!0]!!!!!!!!!$.,!!!!!!!$`!!!!!!!!!!!!!!!!!!!!``````````````````````!!!%!0```````````````````````````````````````````S1E*#1E*#1E````!!!!`Q!!!0``!!!!`Q!!!0``````*#1E*#1E*#4`````!0``!0```Q$``````Q$```````````]E*0````````]!``]!!0```Q!!````!0```````````S1E`````````Q$``Q$```````]!``]!````````````*#4`````````!0``!!!!`Q!!!0```Q$```````````]E*0```````````````````````````````````Q!!!.04!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!U^-!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!$4UQ!!!!!!!0;D?@A!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!$WRXCCH-;&gt;6@9!!!!!!!!!!!!!!0``!!!!!!!!!!!!^K.Y?+*Y?(BYRK.Z+Q!!!!!!!!!!``]!!!!!!!!!!0;D?(BYIHBY?(BY?(C=RY!!!!!!!!$``Q!!!!!!!!!!IXBY?(CC?(BY?(BY?(C=TQ!!!!!!!0``!!!!!!!!!+2Y?(BY?+*Y?(BY?(BYH-@'!!!!!!!!``]!!!!!!!#FH(BY?(BYIHBY?(BY?*T(?-9!!!!!!!$``Q!!!!!!!-@(R]?=?(CC?(BY?(BYRZRYRA!!!!!!!0``!!!!!!!!RZS=H-&lt;(R]:Y?(BY?-?=?(D'!!!!!!!!``]!!!!!!!#DH*S=H*S=RM@(RHD(H(BY?-9!!!!!!!$``Q!!!!!!!+/=H*S=H++=H*T'TJRY?(BYRA!!!!!!!0``!!!!!!!!IZS=H*SCH*S=H*T(H(BY?(CD!!!!!!!!``]!!!!!!!#DH*S=IJS=H*S=H-?CIK*Y?+-!!!!!!!$``Q!!!!!!!+/=H++=H*S=H*S=RZRYIK+CIQ!!!!!!!0``!!!!!!!!IZSCH*S=H*S=H*T(H(BY?*RZ!!!!!!!!``]!!!!!!!#DIJS=H*S=H*S=H-?=?(C=?1!!!!!!!!$``Q!!!!!!!++=H*S=H*S=H*S=RZRYH+-!!!!!!!!!!0``!!!!!!!!_)$(IJS=H*S=H*T(?(CD!!!!!!!!!!!!``]!!!!!!!!!!!$YA-@'H*S=H-&gt;YI`9!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!0B[R];=R]9L!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!+XKL+Q!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$```````````````````````````````````````````]!!!!-!!&amp;'5%B1!!!!!!!$!!!$S!!!#(FYH+V73UQ5723^L[D'V^$%6SA$H5#K[(H&gt;%I7%U7(]D5KE.+)'G;#*HU2NK7:A1I9*(_.+*[2#!AFO3&amp;C9O'8DQA5,N]:UX*1,6CS'25?DTCT:%!VD&gt;8F@66&gt;8&gt;1/.#VF5+JVTXLHXHP.O!5#H7*/5BY=W%,;",Z&gt;NK$%M!J$NI&amp;$Y3]U!OU&lt;_"\)`4GSY1K_R.3F0GGW)'6;+&gt;OKTM&amp;Z!ES\J&amp;(O,U'I7R].K&lt;.BL7)X+2:ZD`(ETHYXYJSL1QO:*8LL/V5^UXBR$14$&lt;R&amp;0J)(EA?KMMGYH_^*]:EYN@IRUU\BY:N9(J6NUYTRX!%V([F8OEV%YY3B?/"$SS$:;8FQ/3YJ&amp;3&lt;BH(E%/Y7WZ\"5[^&lt;L6-]&amp;S\S[FR/;D4Z?PI;HZ"V#Z)Z&gt;2^OM71CLQ(`GCYL[7L&gt;P?WP'%&lt;^P0=72KH&lt;YV`)H7`G3_!!-H?I=[K-S`Y3J^QQ=87IR(*%Y1.Y0O!$4_:FH18:+'F1B0=AW?O$&lt;*PQR'UA:RX&lt;9DKMQQ#(R:W]5'8MYG7HN'JC=H-O$9WJ!W/JC=GN,`'2_[H*T/;E:Z-?Q[RJ&amp;=&lt;MI#X%7QC'4&lt;N:^W+(B5$%@J?8BJ!BD-Q%T:A$*;7FH!G_!SI85BNZ,EC4U'V(JALGG!%QR3&amp;"-0]"9?:P!(*G["`PPK\''PSB&amp;1;ZDN"G)^DG"678FR*G%^_HT$P-KJ@-890S`).UX!QM'K&lt;L*\S3%'_%1^Q%K9L=%YDZV%YX]C:2M\#\PE_MS8@AFO7\]8&amp;R@*]&gt;R=N]6)O%_+FX&amp;RRPDB@2.&lt;8H$=1B]?BL.^#HQQL!F!QRVFR`H-WY1`8H$W_/?E&gt;)X[WMDN;.8ARL_PN[^8/DYRGN0\UZ($:YE'&amp;?RD%QTR87Z,B/ED"8&lt;^R2X%&lt;X^T=R-&lt;R[3G9L@!X*I#,8SJZ(\.B%.=53BTS?IA*C3;)1!*KP5EK+N)F1;`3MKL89!V5GZJY12W%M%3BFBB.C).XADK/5Y47#GB5$@QV&gt;#O#VZ6Y&gt;&lt;TU&lt;U1+9PYM.X;+2G;&lt;[XI1D&amp;WO[R5&lt;BASL/AJ[4BU)/;D!%(9`\6IN_6-;2IF[HGM)@8E1W"SORRW58.K^A'AOJ'21^L^&lt;"\54^//X1T^]/`2^S@DRGITA.&gt;%`R\K^H67&amp;/WPGNG!(C%PBX@&lt;/Q]GJG3JF(18H:&amp;87T&amp;9CY7=]%?O`?K%4R762A@SDT+0L+.98WANU\@65]7.0"\DO6!+Q=VD")RM;^*\[F[R8R*HWUH0M+;Z2X+A`U+&gt;U)\N;`(=A?\H).&amp;&gt;ADDTA4V4;^"7;KU]7!!!!"!!!!#U!!!!%!!!!!!!!!!Q!!5*%3&amp;!!!!!!!!-!!!"F!!!!&gt;8C=9W"A+"3190L(50?8A5HA+Z!B`:?"7&gt;#0]4=$![?@Q'%AT3AA#237`=P!,KA.&amp;N9_IMP"!!7K&lt;)Q=EBS("4H!-BQN'AT```^8+7,E_(LE'FT2%2]Y5W770)=%!'5)':I!!!!!!!!%!!!!"Q!!!KA!!!!(!!!!)6^O;6^-98.U3WZP&gt;WZ0&gt;WZJ&lt;G&gt;-6E.M98.T1WRV=X2F=A!!!&amp;Y8!)!!!!!!!1!)!$$`````!!%!!!!!!%)!!!!#!!J!)12/&lt;WZF!!!Q1&amp;!!!1!!*V2F=X2*&lt;GF';7RF5W6D&gt;'FP&lt;E.P&lt;G:J:X6S982J&lt;WYO&lt;(:D&lt;'&amp;T=Q!"!!%!!!!!!!!!'ER71WRB=X.1=GFW982F2'&amp;U962B9E^S:'6S!!!!+2=!A!!!!!!#!!5!"Q!!$!"!!!(`````!!!!!1!"!!!!!1!!!!!!!!!!!!!!'UR71WRB=X.1=GFW982F2'&amp;U962J&lt;76T&gt;'&amp;N=!!!!"E8!)!!!!!!!1!&amp;!!=!!!%!!.BN/,A!!!!!!!!!*ER71WRB=X.1=GFW982F2'&amp;U95RB=X2"=("M;76E6'FN:8.U97VQ!!!!'2=!A!!!!!!"!!5!"Q!!!1!!W'UYO!!!!!!!!!!;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B6(FQ:52F=W-!!!"K&amp;Q#!!!!!!!%!#!!Q`````Q!"!!!!!!"/!!!!!A!71$,`````$5F/33"';7RF)&amp;"B&gt;'A!-%"1!!%!!#&gt;5:8.U37ZJ2GFM:6.F9X2J&lt;WZ$&lt;WZG;7&gt;V=G&amp;U;7^O,GRW9WRB=X-!!1!"!!!!!!!!!"Z-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;%:GRU2'&amp;U96.J?G5!!!!:&amp;Q#!!!!!!!%!"1!$!!!"!!!!!!!%!!!!!!!!!"J-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;%:GRU2'&amp;U91!!!')8!)!!!!!!!A!71$,`````$5F/33"';7RF)&amp;"B&gt;'A!-%"1!!%!!#&gt;5:8.U37ZJ2GFM:6.F9X2J&lt;WZ$&lt;WZG;7&gt;V=G&amp;U;7^O,GRW9WRB=X-!!1!"5&amp;2)-!!!!!1!!!!!!!!!!!!!!!1!"!!)!!!!"!!!!'E!!!!I!!!!!A!!"!!!!!!O!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1Q!!!'L?*S.D]V+QV!1B&lt;`E^C?ND&gt;;`\I1LAI),&amp;\JR'3A5MJ'A0I!B0T51%UFOCSPRM8Q5(U/@1#&gt;JR95,P1='TJG:=_=!%S&lt;?_;=]V\`S^3T,%RW%ZJZH0NY?,F]"__1WK9V@:%XT*IF-6B&lt;4MECT_;)+'X+7,[-]L/ODP_9CEX0K"6BQ--U8N5EK8;;[8&gt;;06&lt;9-4;,DU)2U"1S*Q8L(&amp;K,7F!M[$,Q_+MXH/.[,+K`.3ED+F,'Y+XLU=&gt;1C4H&amp;IMT&amp;K@RUK]R3,4S#7!T9Y`O=N.F*[4:&amp;JBTMZI].-&lt;$3&lt;&lt;%G\Q6CU&lt;^BL`&amp;:_/NMN6K%M&gt;E2J8!`F,%MC&gt;"HBMMO?6*&gt;^_!)J#&amp;3#!!!!&gt;Q!"!!)!!Q!&amp;!!!!7!!0"!!!!!!0!.A!V1!!!'%!$Q1!!!!!$Q$9!.5!!!"K!!]%!!!!!!]!W!$6!!!!=Y!!B!#!!!!0!.A!V1!!!(7!!)1!A!!!$Q$9!.5)5W6H&lt;W5A65E)5W6H&lt;W5A65E)5W6H&lt;W5A65E"-A%Q!&amp;*45E-.#A!$4&amp;:$1UR#6F=!!"09!!!%9!!!!#!!!"/Y!!!!!!!!!!!!!!!A!!!!.!!!"$Q!!!!=4%F#4A!!!!!!!!&amp;A4&amp;:45A!!!!!!!!&amp;U5F242Q!!!!!!!!')1U.46!!!!!!!!!'=4%FW;1!!!!!!!!'Q1U^/5!!!!!!!!!(%6%UY-!!!!!!!!!(92%:%5Q!!!!!!!!(M4%FE=Q!!!!!!!!)!6EF$2!!!!!%!!!)5&gt;G6S=Q!!!!1!!!)]5U.45A!!!!!!!!+A2U.15A!!!!!!!!+U35.04A!!!!!!!!,);7.M.!!!!!!!!!,=;7.M/!!!!!!!!!,Q4%FG=!!!!!!!!!-%2F")9A!!!!!!!!-92F"421!!!!!!!!-M6F"%5!!!!!!!!!.!4%FC:!!!!!!!!!.51E2)9A!!!!!!!!.I1E2421!!!!!!!!.]6EF55Q!!!!!!!!/12&amp;2)5!!!!!!!!!/E466*2!!!!!!!!!/Y3%F46!!!!!!!!!0-6E.55!!!!!!!!!0A2F2"1A!!!!!!!!0U!!!!!0````]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!Q!!!!!!!!!!$`````!!!!!!!!!.1!!!!!!!!!!0````]!!!!!!!!![!!!!!!!!!!!`````Q!!!!!!!!$Q!!!!!!!!!!$`````!!!!!!!!!2Q!!!!!!!!!!0````]!!!!!!!!"*!!!!!!!!!!!`````Q!!!!!!!!&amp;)!!!!!!!!!!$`````!!!!!!!!!:1!!!!!!!!!!0````]!!!!!!!!"J!!!!!!!!!!"`````Q!!!!!!!!-M!!!!!!!!!!4`````!!!!!!!!!]!!!!!!!!!!"`````]!!!!!!!!$V!!!!!!!!!!)`````Q!!!!!!!!0E!!!!!!!!!!H`````!!!!!!!!!`A!!!!!!!!!#P````]!!!!!!!!%#!!!!!!!!!!!`````Q!!!!!!!!1=!!!!!!!!!!$`````!!!!!!!!"$1!!!!!!!!!!0````]!!!!!!!!%3!!!!!!!!!!!`````Q!!!!!!!!4-!!!!!!!!!!$`````!!!!!!!!"N!!!!!!!!!!!0````]!!!!!!!!+V!!!!!!!!!!!`````Q!!!!!!!!LE!!!!!!!!!!$`````!!!!!!!!$L!!!!!!!!!!!0````]!!!!!!!!/O!!!!!!!!!!!`````Q!!!!!!!!\!!!!!!!!!!!$`````!!!!!!!!$N!!!!!!!!!!!0````]!!!!!!!!00!!!!!!!!!!!`````Q!!!!!!!!^%!!!!!!!!!!$`````!!!!!!!!%@!!!!!!!!!!!0````]!!!!!!!!2_!!!!!!!!!!!`````Q!!!!!!!")!!!!!!!!!!!$`````!!!!!!!!%CQ!!!!!!!!!A0````]!!!!!!!!40!!!!!!D6'6T&gt;%FO;5:J&lt;'64:7.U;7^O1W^O:GFH&gt;8*B&gt;'FP&lt;CZD&gt;'Q!!!!!</Property>
	<Property Name="NI.LVClass.Geneology" Type="Xml"><String>

<Name></Name>

<Val>!!!!!3&gt;5:8.U37ZJ2GFM:6.F9X2J&lt;WZ$&lt;WZG;7&gt;V=G&amp;U;7^O,GRW9WRB=X.16%AQ!!!!!!!!!!!!!!!"!!%!!!!!!!%!!!!!!A!71$,`````$5F/33"';7RF)&amp;"B&gt;'A!@A$RW'UYO!!!!!)H6'6T&gt;%FO;5:J&lt;'64:7.U;7^O1W^O:GFH&gt;8*B&gt;'FP&lt;CZM&gt;G.M98.T)V2F=X2*&lt;GF';7RF5W6D&gt;'FP&lt;E.P&lt;G:J:X6S982J&lt;WYO9X2M!#J!5!!"!!!&gt;1WRV=X2F=C"P:C"D&lt;'&amp;T=S"Q=GFW982F)'2B&gt;'%!!1!"!!!!!1!!!!"16%AQ!!!!"!!!!!!!!!!!!!!"%&amp;2F=X2$98.F,GRW9WRB=X-!5&amp;2)-!!!!!!!!!!!!"=!A!!!!!!!!!!!!!!&amp;!!!!'&amp;2F&lt;8"M982F6'6T&gt;%.B=W5O&lt;(:D&lt;'&amp;T=Q!!!"*.?62F=X2$98.F,GRW9WRB=X-!!!!:6'6T&gt;%:F=X2P1V"9,5:#-T9O&lt;(:D&lt;'&amp;T=Q!!!#*5:8.U1W^O:GFH&gt;8*B&gt;'FP&lt;F*F:G6S:7ZD:3ZM&gt;G.M98.T!!!!*62F=X2$&lt;WZG;7&gt;V=G&amp;U;7^O6X*J&gt;'6"&lt;G23:7&amp;E,GRW9WRB=X-</Val>

</String>

</Property>
	<Property Name="NI.LVClass.IsTransferClass" Type="Bool">false</Property>
	<Property Name="NI.LVClass.LowestCompatibleVersion" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LVClass.ParentClassLinkInfo" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"H!!!!!2"5:8.U1W&amp;T:3ZM&gt;G.M98.T!&amp;"53$!!!!"*!!!!"A=]&gt;GFM;7)_"G&amp;E:'^O=QV@3EN*)&amp;2P&lt;WRL;82T#6:*)&amp;2F=X2F=AR5:8.U1W&amp;T:3ZM&lt;')16'6T&gt;%.B=W5O&lt;(:D&lt;'&amp;T=Q!!!!!</Property>
	<Item Name="TestIniFileSectionConfiguration.ctl" Type="Class Private Data" URL="TestIniFileSectionConfiguration.ctl">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="protected" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
	</Item>
	<Item Name="setUp.vi" Type="VI" URL="../setUp.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;+!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!J*V2F=X2*&lt;GF';7RF5W6D&gt;'FP&lt;E.P&lt;G:J:X6S982J&lt;WYO&lt;(:D&lt;'&amp;T=Q!.=G6G:8*F&lt;G.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!J*V2F=X2*&lt;GF';7RF5W6D&gt;'FP&lt;E.P&lt;G:J:X6S982J&lt;WYO&lt;(:D&lt;'&amp;T=Q!-=G6G:8*F&lt;G.F)'FO!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!)!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
	</Item>
	<Item Name="tearDown.vi" Type="VI" URL="../tearDown.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;+!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!J*V2F=X2*&lt;GF';7RF5W6D&gt;'FP&lt;E.P&lt;G:J:X6S982J&lt;WYO&lt;(:D&lt;'&amp;T=Q!.=G6G:8*F&lt;G.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!J*V2F=X2*&lt;GF';7RF5W6D&gt;'FP&lt;E.P&lt;G:J:X6S982J&lt;WYO&lt;(:D&lt;'&amp;T=Q!-=G6G:8*F&lt;G.F)'FO!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!E!!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!)!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1342710290</Property>
	</Item>
	<Item Name="Test_WriteBinary_ReadCorrectValue.vi" Type="VI" URL="../Test_WriteBinary_ReadCorrectValue.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;)!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!J*V2F=X2*&lt;GF';7RF5W6D&gt;'FP&lt;E.P&lt;G:J:X6S982J&lt;WYO&lt;(:D&lt;'&amp;T=Q!-6'6T&gt;%.B=W5A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$Z!=!!?!!!J*V2F=X2*&lt;GF';7RF5W6D&gt;'FP&lt;E.P&lt;G:J:X6S982J&lt;WYO&lt;(:D&lt;'&amp;T=Q!,6'6T&gt;%.B=W5A;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
	</Item>
	<Item Name="Test_WriteBoolean_ReadCorrectValue.vi" Type="VI" URL="../Test_WriteBoolean_ReadCorrectValue.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;)!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!J*V2F=X2*&lt;GF';7RF5W6D&gt;'FP&lt;E.P&lt;G:J:X6S982J&lt;WYO&lt;(:D&lt;'&amp;T=Q!-6'6T&gt;%.B=W5A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$Z!=!!?!!!J*V2F=X2*&lt;GF';7RF5W6D&gt;'FP&lt;E.P&lt;G:J:X6S982J&lt;WYO&lt;(:D&lt;'&amp;T=Q!,6'6T&gt;%.B=W5A;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777219</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074541072</Property>
	</Item>
	<Item Name="Test_WriteFloat_ReadCorrectValue.vi" Type="VI" URL="../Test_WriteFloat_ReadCorrectValue.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;)!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!J*V2F=X2*&lt;GF';7RF5W6D&gt;'FP&lt;E.P&lt;G:J:X6S982J&lt;WYO&lt;(:D&lt;'&amp;T=Q!-6'6T&gt;%.B=W5A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$Z!=!!?!!!J*V2F=X2*&lt;GF';7RF5W6D&gt;'FP&lt;E.P&lt;G:J:X6S982J&lt;WYO&lt;(:D&lt;'&amp;T=Q!,6'6T&gt;%.B=W5A;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777219</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074541072</Property>
	</Item>
	<Item Name="Test_WritePath_ReadCorrectValue.vi" Type="VI" URL="../Test_WritePath_ReadCorrectValue.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;)!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!J*V2F=X2*&lt;GF';7RF5W6D&gt;'FP&lt;E.P&lt;G:J:X6S982J&lt;WYO&lt;(:D&lt;'&amp;T=Q!-6'6T&gt;%.B=W5A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$Z!=!!?!!!J*V2F=X2*&lt;GF';7RF5W6D&gt;'FP&lt;E.P&lt;G:J:X6S982J&lt;WYO&lt;(:D&lt;'&amp;T=Q!,6'6T&gt;%.B=W5A;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777219</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074541072</Property>
	</Item>
	<Item Name="Test_WriteSignedInteger_ReadCorrectValue.vi" Type="VI" URL="../Test_WriteSignedInteger_ReadCorrectValue.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;)!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!J*V2F=X2*&lt;GF';7RF5W6D&gt;'FP&lt;E.P&lt;G:J:X6S982J&lt;WYO&lt;(:D&lt;'&amp;T=Q!-6'6T&gt;%.B=W5A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$Z!=!!?!!!J*V2F=X2*&lt;GF';7RF5W6D&gt;'FP&lt;E.P&lt;G:J:X6S982J&lt;WYO&lt;(:D&lt;'&amp;T=Q!,6'6T&gt;%.B=W5A;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777219</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074541072</Property>
	</Item>
	<Item Name="Test_WriteSignedInteger_RequestAsUnsigned_ErrorOut.vi" Type="VI" URL="../Test_WriteSignedInteger_RequestAsUnsigned_ErrorOut.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;)!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!J*V2F=X2*&lt;GF';7RF5W6D&gt;'FP&lt;E.P&lt;G:J:X6S982J&lt;WYO&lt;(:D&lt;'&amp;T=Q!-6'6T&gt;%.B=W5A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$Z!=!!?!!!J*V2F=X2*&lt;GF';7RF5W6D&gt;'FP&lt;E.P&lt;G:J:X6S982J&lt;WYO&lt;(:D&lt;'&amp;T=Q!,6'6T&gt;%.B=W5A;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!*!!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
	</Item>
	<Item Name="Test_WriteString_ReadCorrectValue.vi" Type="VI" URL="../Test_WriteString_ReadCorrectValue.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;)!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!J*V2F=X2*&lt;GF';7RF5W6D&gt;'FP&lt;E.P&lt;G:J:X6S982J&lt;WYO&lt;(:D&lt;'&amp;T=Q!-6'6T&gt;%.B=W5A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$Z!=!!?!!!J*V2F=X2*&lt;GF';7RF5W6D&gt;'FP&lt;E.P&lt;G:J:X6S982J&lt;WYO&lt;(:D&lt;'&amp;T=Q!,6'6T&gt;%.B=W5A;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777219</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074541072</Property>
	</Item>
	<Item Name="Test_WriteTimestamp_1AM_ReadCorrectValue.vi" Type="VI" URL="../Test_WriteTimestamp_1AM_ReadCorrectValue.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;)!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!J*V2F=X2*&lt;GF';7RF5W6D&gt;'FP&lt;E.P&lt;G:J:X6S982J&lt;WYO&lt;(:D&lt;'&amp;T=Q!-6'6T&gt;%.B=W5A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$Z!=!!?!!!J*V2F=X2*&lt;GF';7RF5W6D&gt;'FP&lt;E.P&lt;G:J:X6S982J&lt;WYO&lt;(:D&lt;'&amp;T=Q!,6'6T&gt;%.B=W5A;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777219</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074541072</Property>
	</Item>
	<Item Name="Test_WriteTimestamp_1PM_ReadCorrectValue.vi" Type="VI" URL="../Test_WriteTimestamp_1PM_ReadCorrectValue.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;)!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!J*V2F=X2*&lt;GF';7RF5W6D&gt;'FP&lt;E.P&lt;G:J:X6S982J&lt;WYO&lt;(:D&lt;'&amp;T=Q!-6'6T&gt;%.B=W5A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$Z!=!!?!!!J*V2F=X2*&lt;GF';7RF5W6D&gt;'FP&lt;E.P&lt;G:J:X6S982J&lt;WYO&lt;(:D&lt;'&amp;T=Q!,6'6T&gt;%.B=W5A;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777219</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074541072</Property>
	</Item>
	<Item Name="Test_WriteTimestamp_12AM_ReadCorrectValue.vi" Type="VI" URL="../Test_WriteTimestamp_12AM_ReadCorrectValue.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;)!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!J*V2F=X2*&lt;GF';7RF5W6D&gt;'FP&lt;E.P&lt;G:J:X6S982J&lt;WYO&lt;(:D&lt;'&amp;T=Q!-6'6T&gt;%.B=W5A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$Z!=!!?!!!J*V2F=X2*&lt;GF';7RF5W6D&gt;'FP&lt;E.P&lt;G:J:X6S982J&lt;WYO&lt;(:D&lt;'&amp;T=Q!,6'6T&gt;%.B=W5A;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777219</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074541072</Property>
	</Item>
	<Item Name="Test_WriteTimestamp_12PM_ReadCorrectValue.vi" Type="VI" URL="../Test_WriteTimestamp_12PM_ReadCorrectValue.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;)!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!J*V2F=X2*&lt;GF';7RF5W6D&gt;'FP&lt;E.P&lt;G:J:X6S982J&lt;WYO&lt;(:D&lt;'&amp;T=Q!-6'6T&gt;%.B=W5A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$Z!=!!?!!!J*V2F=X2*&lt;GF';7RF5W6D&gt;'FP&lt;E.P&lt;G:J:X6S982J&lt;WYO&lt;(:D&lt;'&amp;T=Q!,6'6T&gt;%.B=W5A;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777219</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1611411984</Property>
	</Item>
	<Item Name="Test_WriteUnsignedInteger_ReadCorrectValue.vi" Type="VI" URL="../Test_WriteUnsignedInteger_ReadCorrectValue.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;)!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!J*V2F=X2*&lt;GF';7RF5W6D&gt;'FP&lt;E.P&lt;G:J:X6S982J&lt;WYO&lt;(:D&lt;'&amp;T=Q!-6'6T&gt;%.B=W5A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$Z!=!!?!!!J*V2F=X2*&lt;GF';7RF5W6D&gt;'FP&lt;E.P&lt;G:J:X6S982J&lt;WYO&lt;(:D&lt;'&amp;T=Q!,6'6T&gt;%.B=W5A;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777219</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074541072</Property>
	</Item>
	<Item Name="Test_WriteUnsignedInteger_RequestAsSigned_NoError.vi" Type="VI" URL="../Test_WriteUnsignedInteger_RequestAsSigned_NoError.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;)!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!J*V2F=X2*&lt;GF';7RF5W6D&gt;'FP&lt;E.P&lt;G:J:X6S982J&lt;WYO&lt;(:D&lt;'&amp;T=Q!-6'6T&gt;%.B=W5A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$Z!=!!?!!!J*V2F=X2*&lt;GF';7RF5W6D&gt;'FP&lt;E.P&lt;G:J:X6S982J&lt;WYO&lt;(:D&lt;'&amp;T=Q!,6'6T&gt;%.B=W5A;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
	</Item>
</LVClass>
