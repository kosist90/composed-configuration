# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
### Changed
### Deprecated
### Removed
### Fixed
### Security

## [1.2.0]
### Added
- New method to Preload Transient Configuration (required for unit tests--the main use case for TransientConfiguration--that depend on a configuration object (rather than on an open configuration reference)

## [1.1.0] - 2019-04-09
### Added
- TransientConfiguration concrete type (i.e. not persisting to disk) for use as a test double in any unit tests that depend on configuration

## [1.0.2] - 2019-03-01
### Added
- IniFileSection class now writes and retrieves timestamp config as time-formatted string
### Fixed
- Minor cleanups and fixes from static analysis results and adding source code to CI

## [1.0.1] - 2019-01-23
### Changed
- LabVIEW Version number (is *actually* 2017 **SP1**, but only major is currently supported)

## [1.0.0] - 2019-01-23
### Release Note
This version is shippable. The public API is settled and the test suite provides reasonable coverage.
### Added
- Write configuration API
- Full **IniFileSection** implementation (read and write)
- **Initialize/Close Configuration Resource** interface methods invoked at reference open/close
- Support for binary data type and defaulting to binary when class provides no type-specific implementation
### Changed
- Numeric types allowing implementation condendensed to Float/Int/Uint
- Read/Write interface methods wrapped and protected to prevent accidental misuse

## [0.0.1] - 2019-01-13
### Added
- Type assertion to mostly enforce correct API use (a stand-in for unsupported Community-scoped malleable VI)
### Changed
- A little project organization and documentation to clarify intended use

## [0.0.0] - 2019-01-11
### Added
- Everything (Initial publication to test GPM)